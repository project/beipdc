<?php

/**
 * @file
 *
 * Class implemenation for IPDC product migrate.
 */

class BEIpdcListXML extends MigrateListXML {
  public function __construct($item_url) {
    parent::__construct($item_url);
  }

  protected function getIDsFromXML(SimpleXMLElement $xml) {
    $ids = array();
    foreach ($xml->product as $product) {
      $ids[] = (string)$product['id'];
    }
    return array_unique($ids);
  }
}

class IPDCProducts extends XMLMigration {
  protected function applyMappings() {
    // We only know what data to pull from the xpaths in the mappings.
    foreach ($this->fieldMappings as $mapping) {
      $source = $mapping->getSourceField();

      if ($source) {
        $xpath = $mapping->getXpath();
        if ($xpath) {
          // Derived class may override applyXpath()
          $values = $this->applyXpath($this->sourceValues, $xpath);
          $this->sourceValues->$source = $this->applyXpath($this->sourceValues, $xpath);
        }
      }
    }

    // Override destination value for website.
    $website_destination = array();

    $field_mapping = beipdc_get_field_mapping();
    foreach ($field_mapping as $source_field => $field_detail) {
      if (empty($this->sourceValues->$source_field) || !isset($field_detail['field_type'])) {
        continue;
      }

      $values = &$this->sourceValues->$source_field;
      $field = $field_detail['field'];

      switch ($field_detail['field_type']) {
        case 'term' :
        case 'node' :
          $count = 0;
          $find_value = TRUE;
          if ($field_detail['field_type'] == 'term' && variable_get('beipdc_' . $field, 1) == 1) {
            $find_value = FALSE;
          }

          if (is_array($values)) {
            $formatted_values = array();
            foreach ($values as $value) {
              $value = $find_value ?
                beipdc_get_field_value($source_field, $value, $field_detail) : $value;
              if ($value) {
                $formatted_values[$count++] = $value;
              }
            }
            $values = $formatted_values;
          }
          else {
            $values = $find_value ?
              beipdc_get_field_value($source_field, $values, $field_detail) : $values;
          }
          break;

        case 'link' :
          $url_config = variable_get('beipdc_field_ipdc_websites', 2);
          $migration = Migration::currentMigration();

          $links = array();
          $titles = array();
          foreach ($this->sourceValues->xml->xpath('product/links/link') as $link) {
            $url = (string)$link->url;

            if ($url_config > 1 && !_beipdc_is_valid_url($url)) {
              if ($url_config == 2) {
                $migration->saveMessage(
                  t('Url !url for field !field exceeds is not valid, hence skipping the url.', array(
                    '!url' => $url,
                    '!field' => 'field_ipdc_websites',
                  )),
                  Migration::MESSAGE_INFORMATIONAL
                );
                continue;
              }
              else {
                $migration->saveMessage(
                  t('Url !url for field !field exceeds is not valid.', array(
                    '!url' => $url,
                    '!field' => 'field_ipdc_websites',
                  )),
                  Migration::MESSAGE_INFORMATIONAL
                );
              }
            }

            $links[] = $url;
            $titles[] = (string)$link->titel;
          }

          $values = $links;
          $links['arguments']['title'] = $titles;
          $website_destination = $links;
        break;
      }
    }

    Migration::applyMappings();

    // Small hack to provide title field for website.
    if (!empty($website_destination)) {
      $this->destinationValues->field_ipdc_websites = $website_destination;
    }
  }

  protected function import() {
    parent::import();

    $ipdc_url = beipdc_get_ipdc_url();

    $list_url = $ipdc_url . '/ZoekProducten';
    $use_date = variable_get('beipdc_use_date', NULL);
    if ($use_date) {
      $list_url .= '?LastModified=' . $use_date;
    }
    Migration::displayMessage(t('Url !url was used for migration.', array('!url' => $list_url)), 'status');

    variable_set('beipdc_use_date', date("c", strtotime("yesterday")));
    variable_set('beipdc_last_update', date('Y-m-d'));
    Migration::displayMessage(t('Next update date set to !date.', array('!date' => variable_get('beipdc_use_date'))) , 'status');
  }

  public function endProcess() {
    if ($this->status == Migration::STATUS_ROLLING_BACK) {
      variable_set('beipdc_use_date', NULL);
      variable_set('beipdc_last_update', date('Y-m-d'));
      Migration::displayMessage(t('Next update date reset to NULL.'), 'status');
    }
    parent::endProcess();
  }

  public function __construct() {
    parent::__construct(MigrateGroup::getInstance('IPDCProducts'));

    $this->description = t('Import IPDC products from XML source');

    $field_mapping = beipdc_get_field_mapping();
    $field_title = array();

    foreach ($field_mapping as $source_name => $field_detail) {
      $field_title[$source_name] = $field_detail['field_title'];
    }

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'productId' => array(
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    foreach ($field_mapping as $xml_field => &$field_details) {
      $path = "product";

      if (isset($field_details['value_path'])) {
        $path .= '/' . $field_details['value_path'];
      }
      else {
        $path .= '/' . $xml_field;
      }

      if (isset($field_details['field_type'])) {
        if ($field_details['field_type'] == 'term') {
          $field_details['arguments'] = variable_get('beipdc_' . $field_details['field'], 1) ?
            array('create_term' => TRUE, 'ignore_case' => TRUE) : array('source_type' => 'tid');
        }
      }

      $fieldMap =  $this->addFieldMapping($field_details['field'], $xml_field)
        ->xpath($path);

      if (isset($field_details['arguments'])) {
        $fieldMap->arguments($field_details['arguments']);
      }
    }

    // Status field
    $node_status = variable_get('beipdc_status') ? 1 : 0;
    $this->addFieldMapping('status')->defaultValue($node_status);

    // Creator user id.
    $node_uid = variable_get('beipdc_user', 1);
    $node_uid = !is_numeric($node_uid) ? 1 : $node_uid;
    $this->addFieldMapping('uid')->defaultValue($node_uid);

    if (module_exists('path')) {
      $this->addFieldMapping('path')
        ->issueGroup(t('DNM'));
      if (module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
          ->issueGroup(t('DNM'));
      }
    }

    $ipdc_url = beipdc_get_ipdc_url();

    $list_url = $ipdc_url . '/ZoekProducten';
    $use_date = variable_get('beipdc_use_date', NULL);
    if ($use_date) {
      $list_url .= '?LastModified=' . $use_date;
    }

    $item_url = $ipdc_url . '/GeefProduct/:id';

    $this->source = new MigrateSourceList(new BEIpdcListXML($list_url),
                                          new MigrateItemXML($item_url, $field_mapping), $field_title);

    $this->destination = new MigrateDestinationNode(BEIPDC_CONTENT_TYPE);
  }

}
