<?php

/**
 * @file
 *
 * Admin setting for IPDC import.
 */

/**
 * Admin configuration for IPDC.
 */
function beipdc_admin_settings($form, $form_state) {
  $taxonomy_fields = array(
    'field_ipdc_doelgroep' => t('Doelgroep terms'),
    'field_ipdc_product_type' => t('Product type terms'),
    'field_ipdc_geografisch' => t('Geografisch Toepassingsgebied terms'),
    'field_ipdc_trefwoorden' => t('Trefwoorden terms'),
    'field_ipdc_thema' => t('Thema terms'),
  );

  $form['beipdc_url'] = array(
    '#type' => 'textfield',
    '#title' => t('IPDC base webservice url'),
    '#required' => TRUE,
    '#default_value' => variable_get('beipdc_url', BEIPDC_DEFAULT_WEBSERVICE_URL),
  );

  $form['beipdc_uuid'] = array(
    '#type' => 'textfield',
    '#title' => t('IPDC user ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('beipdc_uuid', ''),
  );

  $form['beipdc_status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Publish all created IPDC product'),
    '#default_value' => variable_get('beipdc_status'),
  );

  $form['beipdc_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Creator user ID'),
    '#required' => TRUE,
    '#size' => 20,
    '#default_value' => variable_get('beipdc_user', 1),
  );

  foreach ($taxonomy_fields as $field => $title) {
    $form['beipdc_' . $field] = array(
      '#type' => 'select',
      '#title' => $title,
      '#options' => array(1 => t('Add non-existing terms'), 0 => t('Ignore non-existing terms')),
      '#default_value' => variable_get('beipdc_' . $field, 1),
    );
  }

  $form['beipdc_field_ipdc_websites'] = array(
   '#type' => 'radios',
   '#title' => t('Website url'),
   '#options' => array(1 => "Don't verify", 2 => "Verify and ignore if failing", 3 => "Verify and import, even if failing"),
   '#default_value' => variable_get('beipdc_field_ipdc_websites', 2),
  );

  return system_settings_form($form);
}

/**
 *  Sync taxonomycon for IPDC.
 */
function beipdc_admin_sync($form, $form_state) {
  $form['sync_taxonomy'] = array(
    '#type' => 'submit',
    '#value' => t('Sync taxonomy'),
  );

  return $form;
}

/**
 * Submit function for sync taxonomy.
 */
function beipdc_admin_sync_submit($form, $form_state) {
  $url = beipdc_get_ipdc_url();
  $url .= '/GeefTaxonomie';
  $xml = simplexml_load_file($url);
  if (!$xml) {
    drupal_set_message(t('Unable to read data from @url', array('@url' => $url)), 'error');
    return;
  }

  $field_mapping = beipdc_get_field_mapping();
  $voc_updated = array();

  foreach ($field_mapping as $xml_field => $field_details) {
    if (!isset($field_details['field_type']) || $field_details['field_type'] != 'term') {
      continue;
    }

    $path = '';
    if (isset($field_details['sync_path'])) {
      $path = $field_details['sync_path'];
    }
    elseif (isset($field_details['value_path'])) {
      $path = $field_details['value_path'];
    }
    else {
      $path = $xml_field;
    }

    $result = $xml->xpath($path);
    if (!$result) {
      continue;
    }

    $value = NULL;
    if (count($result) > 1) {
      $return = array();
      foreach ($result as $record) {
        $return[] = (string)$record;
      }
      $value = $return;
    }
    else {
      $value = (string)$result[0];
    }

    if (!is_array($value)) {
      $value = array($value);
    }

    $vocabulary = taxonomy_vocabulary_machine_name_load($field_details['voc']);
    if (empty($vocabulary)) {
      continue;
    }

    $value = array_unique($value);
    $add_terms = array();
    foreach ($value as $term) {
      if (!beipdc_get_field_value($xml_field, $term, $field_details)) {
        $voc_updated[$vocabulary->name] = $vocabulary->name;
        taxonomy_term_save((object) array(
          'name' => $term,
          'vid' => $vocabulary->vid,
        ));
      }
    }
  }

  drupal_set_message(t('Taxonomy has been synchronized successfully!'));
  if (!empty($voc_updated)) {
    drupal_set_message(t('One or more terms has been added for vocabulary @voc.', array('@voc' => implode(', ', $voc_updated))));
  }
}
