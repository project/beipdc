<?php
/**
 * @file
 * beipdc_products.features.inc
 */

/**
 * Implements hook_node_info().
 */
function beipdc_products_node_info() {
  $items = array(
    'ipdc_product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titel'),
      'help' => '',
    ),
  );
  return $items;
}
