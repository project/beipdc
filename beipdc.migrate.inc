<?php

/**
 * @file.
 *
 * Implements migration api.
 */

/**
 * Implements hook_migrate_api().
 */
function beipdc_migrate_api() {
  $api = array(
    'api' => 2,
    'migrations' => array(
      'IPDCProducts' => array('class_name' => 'IPDCProducts'),
    ),
  );
  return $api;
}
